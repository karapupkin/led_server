#include <string.h>
#include "Command/SetLedColorCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"

namespace command {

SetLedColorCommand::SetLedColorCommand() :
		cmd_name(COMMAND_SET_LED_COLOR)
{
	///empty
}

SetLedColorCommand::~SetLedColorCommand()
{
	///empty
}

const char* SetLedColorCommand::name()
{
	return cmd_name;
}

void SetLedColorCommand::execute(std::vector<const char*>& args, Response& resp)
{
	hardware::LedColor color;

	if(args.size() < 2)
	{
		resp.commandResponse = COMMAND_STATUS_FAILED;
		return;
	}

	if(strcmp(COMMAND_SET_LED_COLOR_RED, args[1]) == 0)
		color = hardware::LED_COLOR_RED;
	else if(strcmp(COMMAND_SET_LED_COLOR_GREEN, args[1]) == 0)
		color = hardware::LED_COLOR_GREEN;
	else if(strcmp(COMMAND_SET_LED_COLOR_BLUE, args[1]) == 0)
		color = hardware::LED_COLOR_BLUE;
	else
	{
		resp.commandResponse = COMMAND_STATUS_FAILED;
		return;
	}

	const bool result = hardware::HAL::instance().setLedColor(color);

	if(result)
		resp.commandResponse = std::string(COMMAND_STATUS_OK);
	else
		resp.commandResponse = std::string(COMMAND_STATUS_FAILED);

}

} //end namespace command


