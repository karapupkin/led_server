#include <string.h>
#include "Command/SetLedRateCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"

namespace command {

SetLedRateCommand::SetLedRateCommand() :
		cmd_name(COMMAND_SET_LED_RATE)
{
	///empty
}

SetLedRateCommand::~SetLedRateCommand()
{
	///empty
}

const char* SetLedRateCommand::name()
{
	return cmd_name;
}
#include "logger.h"
void SetLedRateCommand::execute(std::vector<const char*>& args, Response& resp)
{
	if(args.size() < 2)
	{
		LOG_INFO("first check");
		resp.commandResponse = COMMAND_STATUS_FAILED;
		return;
	}

	size_t rate = 0;

	LOG_INFO("second check %zu  %d",strlen(args[1]),isdigit(args[1][0]));
	if(strlen(args[1]) == 1 && isdigit(args[1][0]))
	{
		rate = atoi (args[1]);
		const bool result = hardware::HAL::instance().setLedRate(rate);

		if(result)
		{
			resp.commandResponse = COMMAND_STATUS_OK;
			return;
		}
	}
	LOG_INFO("end func");
	resp.commandResponse = COMMAND_STATUS_FAILED;
}

} //end namespace command


