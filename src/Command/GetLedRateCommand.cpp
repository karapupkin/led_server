#include "Command/GetLedRateCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"
#include <sstream>

namespace command {

GetLedRateCommand::GetLedRateCommand() :
		cmd_name(COMMAND_GET_LED_RATE)
{
	///empty
}

GetLedRateCommand::~GetLedRateCommand()
{
	///empty
}

const char* GetLedRateCommand::name()
{
	return cmd_name;
}

void GetLedRateCommand::execute(std::vector<const char*>& args, Response& resp)
{
	size_t rate = hardware::HAL::instance().getLedRate();
	std::ostringstream ostr;
	ostr << COMMAND_STATUS_OK <<" " << rate;
	resp.commandResponse = ostr.str();
}

} //end namespace command


