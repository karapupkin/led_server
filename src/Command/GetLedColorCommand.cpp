#include "Command/GetLedColorCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"
#include "logger.h"

namespace command {

GetLedColorCommand::GetLedColorCommand() :
		cmd_name(COMMAND_GET_LED_COLOR)
{
	///empty
}

GetLedColorCommand::~GetLedColorCommand()
{
	///empty
}

const char* GetLedColorCommand::name()
{
	return cmd_name;
}

void GetLedColorCommand::execute(std::vector<const char*>& args, Response& resp)
{

	hardware::LedColor color = hardware::HAL::instance().getLedColor();

	switch (color)
	{
	case hardware::LED_COLOR_RED:
		resp.commandResponse = std::string(COMMAND_STATUS_OK) + " " + COMMAND_SET_LED_COLOR_RED;
		break;
	case hardware::LED_COLOR_GREEN:
		resp.commandResponse = std::string(COMMAND_STATUS_OK) + " " + COMMAND_SET_LED_COLOR_GREEN;
		break;
	case hardware::LED_COLOR_BLUE:
		resp.commandResponse = std::string(COMMAND_STATUS_OK) + " " + COMMAND_SET_LED_COLOR_BLUE;
		break;
	default:
		resp.commandResponse = COMMAND_STATUS_FAILED;
	}
}

} //end namespace command




