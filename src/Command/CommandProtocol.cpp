#include "Command/CommandProtocol.h"

#include "Command/CommandManager.h"
#include "Command/CommandStringParser.h"
#include "Command/CommandResponse.h"
#include "logger.h"
#include <string>

namespace command
{

CommandProtocol::CommandProtocol()
{
	///empty
}

CommandProtocol::~CommandProtocol()
{
	///empty
}

bool CommandProtocol::getCommand(std::vector<char> & buffer, size_t& offset, std::vector<char> &response_addr, std::vector<char> &command)
{
	if (offset >= buffer.size())
	{
		return false;
	}

	if (!parse_protocol_command(buffer, offset, command, response_addr))
	{
		return false;
	}
	return true;
}

}  //end namespace command
