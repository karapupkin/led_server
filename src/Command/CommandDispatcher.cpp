#include <stdlib.h>
#include <exception>
#include "Command/CommandDispatcher.h"
#include "Command/CommandManager.h"
#include "Command/CommandResponse.h"
#include "logger.h"

namespace command
{

CommandDispatcher::CommandDispatcher() :
	commandManager(new CommandManager)
{
	messageQueue.start();
}

CommandDispatcher::~CommandDispatcher()
{
	messageQueue.stop();
}

bool CommandDispatcher::addMessage(std::unique_ptr<net::Message>& mes)
{
	return messageQueue.push(mes);
}

void CommandDispatcher::main_loop()
{
	LOG_INFO("CommandDispatcher loop start");
	while ( !stopped )
	{
		try {
			MessageQueue::QueueElement element;

			element = messageQueue.get_wait(100000);

			if (!element)
				continue;

			Response resp;

			commandManager->executeCommand(element->getData(), resp);

			element->answer(resp);
		}
		catch (std::exception& e)
		{
			LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
			LOG_ERROR("Exception caught: \'%s\'", e.what());
			exit(1);
		}
		catch (...)
		{
			LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
			LOG_ERROR("Unknown exception!");
			exit(1);
		}
	}
	LOG_INFO("CommandDispatcher loop end");
}

}


