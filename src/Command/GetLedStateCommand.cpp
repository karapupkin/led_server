#include "Command/GetLedStateCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"

namespace command {

GetLedStateCommand::GetLedStateCommand() :
		cmd_name(COMMAND_GET_LED_STATE)
{
	///empty
}

GetLedStateCommand::~GetLedStateCommand()
{
	///empty
}

const char* GetLedStateCommand::name()
{
	return cmd_name;
}

void GetLedStateCommand::execute(std::vector<const char*>& args, Response& resp)
{
	hardware::LedState state = hardware::HAL::instance().getLedState();
	resp.commandResponse = COMMAND_STATUS_OK;
	resp.commandResponse += " ";
	resp.commandResponse += (state == hardware::LED_STATE_ON ? COMMAND_SET_LED_STATE_ON: COMMAND_SET_LED_STATE_OFF);
}

} //end namespace command


