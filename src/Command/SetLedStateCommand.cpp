#include <string.h>
#include "Command/SetLedStateCommand.h"
#include "Command/CommandDefines.h"
#include "HAL/HardwareAbstractLayer.h"

namespace command {

SetLedStateCommand::SetLedStateCommand() :
		cmd_name(COMMAND_SET_LED_STATE)
{
	///empty
}

SetLedStateCommand::~SetLedStateCommand()
{
	///empty
}

const char* SetLedStateCommand::name()
{
	return cmd_name;
}

void SetLedStateCommand::execute(std::vector<const char*>& args, Response& resp)
{
	if(args.size() < 2)
	{
		resp.commandResponse = COMMAND_STATUS_FAILED;
		return;
	}

	hardware::LedState state;

	if(strcmp(COMMAND_SET_LED_STATE_ON, args[1]) == 0)
		state = hardware::LED_STATE_ON;
	else if(strcmp(COMMAND_SET_LED_STATE_OFF, args[1]) == 0)
		state = hardware::LED_STATE_OFF;
	else
	{
		resp.commandResponse = COMMAND_STATUS_FAILED;
		return;
	}

	const bool result = hardware::HAL::instance().setLedState(state);

	if(result)
		resp.commandResponse = std::string(COMMAND_STATUS_OK);
	else
		resp.commandResponse = std::string(COMMAND_STATUS_FAILED);
}

} //end namespace command


