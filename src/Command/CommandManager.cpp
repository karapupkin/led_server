#include <cassert>

#include "Command/CommandManager.h"
#include "Command/CommandDefines.h"
#include "Command/CommandStringParser.h"
#include "Command/Command.h"
#include "Command/CommandResponse.h"
#include "Command/GetLedColorCommand.h"
#include "Command/GetLedRateCommand.h"
#include "Command/GetLedStateCommand.h"
#include "Command/SetLedColorCommand.h"
#include "Command/SetLedRateCommand.h"
#include "Command/SetLedStateCommand.h"

#include "logger.h"

namespace command
{
/// Macros to insert new command and check on runtime not uniq command name
#define  INSERT_COMMAND(A,B) \
{ \
	std::unique_ptr<Command> temp_cmd(new B);       \
	auto ret = commands.insert(std::make_pair<std::string, std::unique_ptr<Command>>(std::string(temp_cmd->name()), std::move(temp_cmd)));       \
	if (ret.second==false)    \
	{                         \
		LOG_FATAL("Command with ID: \'%s\' exist!", ret.first->second->name());  \
		assert(true);         \
	}                         \
} \



CommandManager::CommandManager()
{
	/// Add all known commands to manager
	INSERT_COMMAND(commands, command::GetLedColorCommand);
	INSERT_COMMAND(commands, command::GetLedRateCommand);
	INSERT_COMMAND(commands, command::GetLedStateCommand);
	INSERT_COMMAND(commands, command::SetLedColorCommand);
	INSERT_COMMAND(commands, command::SetLedRateCommand);
	INSERT_COMMAND(commands, command::SetLedStateCommand);
}

CommandManager::~CommandManager()
{
	///empty
}

bool CommandManager::executeCommand(std::vector<char> & command, Response& resp)
{

	std::vector<const char*> args;
	parse_command_string(command, args);
	if(args.empty())
		return false;

	auto it = commands.find(std::string(args[0]));

	if(it!=commands.end())
	{
		LOG_INFO("Command: %s", it->second->name());
		it->second->execute(args, resp);
		LOG_INFO("Response: %s", resp.commandResponse.c_str());
		return true;
	}
	resp.commandResponse = COMMAND_STATUS_FAILED;
	return false;
}

} //end namespace command
