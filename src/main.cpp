#include <iostream>
#include <iomanip>
#include <list>
#include <thread>
#include <chrono>
#include <atomic>
#include <exception>
#include <signal.h>
#include <stdlib.h>

#include "Command/CommandDispatcher.h"
#include "net/FifoReadServer.h"

#include "logger.h"

std::atomic<bool> applicationShutdown (false);

void signal_callback( int sig )
{
	applicationShutdown.exchange(true);
	LOG_INFO("Signal catch");
}


int main(int argc, char** argv){
	LOG_INFO("Application start");

	// Call sig_callback if user hits ctrl-c
	signal( SIGINT, signal_callback );

	try {
		// Create CommandDispatcher
		command::CommandDispatcher commandDispatcher;

		//Create FIFO Read Server and init it by CommandDispatcher
		net::FifoReadServer fifo_server(commandDispatcher);

		/// Start components
		commandDispatcher.run();
		fifo_server.run();

		while (!applicationShutdown)
		{
			/// wait while app not receive SIGINT
			std::this_thread::sleep_for (std::chrono::seconds(1));
		}

		/// Stop components
		fifo_server.stop();
		commandDispatcher.stop();
	}
	catch (std::exception& e)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Exception caught: \'%s\'", e.what());
		exit(1);
	}
	catch (...)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Unknown exception!");
		exit(1);
	}

	LOG_INFO("Application exit");
	return 0;
}
