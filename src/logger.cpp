#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <atomic>
#include <thread>
#include <sstream>
#include <iostream>
#include <functional>

#include "logger.h"



std::atomic<int> row_number_= ATOMIC_VAR_INIT(0);


extern "C" void print_log(logLevel_e , const char * format, ...)
{
	char buffer[PIPE_BUF];

	char* ptr = buffer;
	size_t remainder = sizeof(buffer) - 1; // for additional "\n"
	timeval time_of_day;
	gettimeofday(&time_of_day, NULL);

	tm local_time;
	localtime_r(&time_of_day.tv_sec, &local_time);

	const uint32_t ms = static_cast<uint32_t>(time_of_day.tv_usec / 1000);

	const int ret1 = snprintf(ptr, remainder, "%.2d.%.2d.%4d %.2d:%.2d:%.2d.%.3u ",
							local_time.tm_mday,
							local_time.tm_mon + 1,
							local_time.tm_year + 1900,
							local_time.tm_hour,
							local_time.tm_min,
							local_time.tm_sec,
							ms);
	ptr += static_cast<size_t>(ret1);
	remainder -= static_cast<size_t>(ret1);

	std::stringstream ss;
	ss << std::this_thread::get_id();

	const int ret2 = snprintf(ptr, remainder, "[%s] :%u ",  ss.str().c_str(), std::atomic_fetch_add(&row_number_, 1));
	ptr += static_cast<size_t>(ret2);
	remainder -= static_cast<size_t>(ret2);
	{
		va_list arg;
		va_start(arg, format);
		const int ret3 = vsnprintf(ptr, remainder, format, arg);
		va_end(arg);
		ptr += static_cast<size_t>(ret3);
		remainder -= static_cast<size_t>(ret3);
	}

	*ptr++ = '\0';
	size_t length = ptr - buffer - 1;

	::write(fileno(stderr), buffer, length);

}


