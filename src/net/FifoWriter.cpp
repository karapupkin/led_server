#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "net/FifoWriter.h"
#include "Command/CommandResponse.h"
#include "logger.h"
#include "util/util.h"

namespace net {

bool FifoWriter::sendResponse(command::Response& response) {
	int fd;

	if ( (fd = open(response_addr.data(),O_RDWR )) <= 0 )
	{
		LOG_ERROR("Can't open fifo: \'%s\'", response_addr.data());
		return false;
	}

	response.commandResponse += '\n';
	const int length = write( fd, response.commandResponse.c_str(), response.commandResponse.length());
	if ( length < 0 ) {
		LOG_ERROR( "Write error: \'%s\' on pipe: \'%s\'", util::str_errno().c_str(), response_addr.data());
		close(fd);
		return false;
	}
	if ( static_cast<size_t>(length) != response.commandResponse.length())
	{
			LOG_ERROR("Written: %d, need write: %zu, fifo: \'%s\'", length, response.commandResponse.length(), response_addr.data());
			close(fd);
			return false;
	}

	close (fd);
	return true;
}

}



