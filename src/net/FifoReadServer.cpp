#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <algorithm>
#include <exception>

#include "net/FifoReadServer.h"
#include "net/FifoMessage.h"
#include "net/FifoWriter.h"
#include "Command/CommandProtocol.h"
#include "Command/CommandResponse.h"
#include "Command/CommandDispatcher.h"
#include "logger.h"
#include "util/util.h"


#define SERVER_FIFO_URL "/tmp/led_server"
#define BUFSIZE        50

namespace net {

FifoReadServer::FifoReadServer(command::CommandDispatcher& dispatcher) :
	stopped{true}
	, commandDispatcher{dispatcher}
{
	/// empty
}

FifoReadServer::~FifoReadServer()
{
	if(!is_stopped())
		stop();
}

void FifoReadServer::main_loop()
{
	try
	{
		LOG_INFO("FifoReadServer loop start");

		command::CommandProtocol commandProtocol;

		unlink(SERVER_FIFO_URL);

		if ( mkfifo(SERVER_FIFO_URL, 0777) ) {
			LOG_ERROR("Can\'t create fifo file: \'%s\'", SERVER_FIFO_URL);
			return;
		}

		int fd;
		if ( (fd = open(SERVER_FIFO_URL, O_RDWR | O_RDONLY)) <= 0 ) {
			LOG_ERROR("Can't open fifo: \'%s\'", SERVER_FIFO_URL);
			return;
		}

		fd_set read_set;
		fd_set except_set;


		std::vector<char> buffer(1000,0);
		size_t current_buffer_size = 0;
		size_t offset = 0;

		// Continue until stopped == false.
		while ( !stopped )
		{
			struct timeval tv;
			tv.tv_sec = 0;
			tv.tv_usec = 100000;

			FD_ZERO( &read_set );
			FD_SET( fd, &read_set );
			FD_ZERO( &except_set );
			FD_SET( fd, &except_set );

			int largest_sock = fd;

			const int ret = select( largest_sock + 1, &read_set, NULL, &except_set, &tv );

			if (ret < 0)
			{
				LOG_ERROR("select() returns %d error: \'%s\'", ret, util::str_errno().c_str());
				util::str_errno();
				continue;
			}

			if (ret == 0)
			{
				// timeout
				continue;
			}

			/// All fd_set's should be checked.
			if (FD_ISSET(fd, &except_set))
			{
				LOG_ERROR("Some error on file!");
				continue;
			}

			/// All fd_set's should be checked.
			if (!FD_ISSET(fd, &read_set))
			{
				continue;
			}

			/// Read event(s) from non-blocking inotify fd (non-blocking specified in inotify_init1 above).
			const int length = read( fd, buffer.data() + offset, buffer.size() - offset);
			if ( length < 0 ) {
				LOG_ERROR( "read error: \'%s\'", util::str_errno().c_str());
			}

			if (length == 0)
			{
				usleep(1000000);
				continue;
			}

			current_buffer_size += length;

			std::vector<char> command;
			std::vector<char> response_addr;
			while (commandProtocol.getCommand(buffer, offset, response_addr, command))
			{
				auto message = std::unique_ptr<Message>(
						new FifoMessage<FifoWriter>(command, std::unique_ptr<FifoWriter>(new FifoWriter(response_addr))));
				commandDispatcher.addMessage(message);
			}

			if (current_buffer_size == offset)
			{
				offset = 0;
				current_buffer_size = 0;
			}

			if (buffer.size() - offset < (buffer.size()/4))
			{
				std::vector<char> temp(1000, 0);

				std::copy_n(buffer.begin() + offset, current_buffer_size - offset, temp.begin());
				buffer.swap(temp);
				current_buffer_size -= offset;
				offset = 0;
			}

		}

		close (fd);
		unlink(SERVER_FIFO_URL);

		LOG_INFO("FifoReadServer loop end");
	}
	catch (std::exception& e)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Exception caught: \'%s\'", e.what());
		exit(1);
	}
	catch (...)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Unknown exception!");
		exit(1);
	}
}

bool FifoReadServer::write_response(std::vector<char> &response_addr, command::Response& resp)
{

	int fd;

	if ( (fd = open(response_addr.data(),O_RDWR )) <= 0 )
	{
		LOG_ERROR("Can't open fifo: \'%s\'", response_addr.data());
		return false;
	}

	resp.commandResponse += '\n';
	const int length = write( fd, resp.commandResponse.c_str(), resp.commandResponse.length());
	if ( length < 0 ) {
		LOG_ERROR( "Write error: \'%s\' on pipe: \'%s\'", util::str_errno().c_str(), response_addr.data());
		close(fd);
		return false;
	}
	if ( static_cast<size_t>(length) != resp.commandResponse.length())
	{
			LOG_ERROR("Written: %d, need write: %zu, fifo: \'%s\'", length, resp.commandResponse.length(), response_addr.data());
			close(fd);
			return false;
	}

	close (fd);
	return true;
}

}


