#include <string.h>

#include "util/util.h"


#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
#define POSIX_STRERROR
#endif

namespace util {

	std::string str_error(int error) {
		char buf[64] = { 0 };
#if POSIX_STRERROR
		if (0 != ::strerror_r(error, buf, sizeof(buf)))
			return std::string("UNKNOWN");
		buf[sizeof(buf)-1] = 0;
		return std::string(buf);
#else
		const char *str = ::strerror_r(error, buf, sizeof(buf));
		if (str == NULL)
			return std::string("UNKNOWN");
		return std::string(str);
#endif
	}

	std::string trim(const std::string &str)
	{
		if(str.length() == 0)
			return str;

		size_t begin_skip = 0;
		while(begin_skip < str.length() && isspace(str[begin_skip]))
			++begin_skip;

		size_t end_skip = str.length();
		while(end_skip-1 > begin_skip && isspace(str[end_skip-1]))
			--end_skip;

		return str.substr(begin_skip, end_skip - begin_skip);
	}

} // namespace util


