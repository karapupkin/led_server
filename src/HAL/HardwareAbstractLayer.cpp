#include <unistd.h>
#include <stdlib.h>
#include <exception>
#include "HAL/HardwareAbstractLayer.h"
#include "logger.h"


namespace hardware {

/// LED parameters visualization
void printLedStatus(enum LedState state, enum LedColor color, size_t rate)
{
	const char *state_str = state == LED_STATE_ON ? "ON" : "OFF";

	const char *color_str = "";
	switch(color)
	{
	case LED_COLOR_RED :
		color_str = "RED";
		break;
	case LED_COLOR_GREEN :
		color_str = "GREEN";
		break;
	case LED_COLOR_BLUE :
		color_str = "BLUE";
		break;
	}

	LOG_INFO("LED state: \'%s\', color: \'%s\', flash rate: %zu", state_str, color_str, rate);
}


HAL::HAL():
		stopped{true}
		, led_state(LED_STATE_OFF)
		, led_color(LED_COLOR_RED)
		, led_rate(0)
{
	run_led_timer_loop();
}

HAL::~HAL()
{
	if(!is_stopped_led_timer_loop())
		stop_led_timer_loop();
}

bool HAL::setLedState(enum LedState state)
{
	std::unique_lock<std::mutex> lck(hal_mutex);
	led_state = state;
	if(state == LED_STATE_OFF)
		led_rate = 0;
	printLedStatus(led_state, led_color, led_rate);
	return true;
}

LedState HAL::getLedState()
{
	std::unique_lock<std::mutex> lck(hal_mutex);
	printLedStatus(led_state, led_color, led_rate);
	return led_state;
}

bool HAL::setLedColor(enum LedColor color)
{
	std::unique_lock<std::mutex> lck(hal_mutex);
	led_color = color;
	printLedStatus(led_state, led_color, led_rate);
	return true;
}

LedColor HAL::getLedColor()
{
	std::unique_lock<std::mutex> lck(hal_mutex);
	printLedStatus(led_state, led_color, led_rate);
	return led_color;
}

bool HAL::setLedRate(size_t rate)
{
	if (rate > 5)
		return false;

	std::unique_lock<std::mutex> lck(hal_mutex);
	led_rate = rate;
	printLedStatus(led_state, led_color, led_rate);

	return true;
}
size_t HAL::getLedRate()
{
	std::unique_lock<std::mutex> lck(hal_mutex);
	printLedStatus(led_state, led_color, led_rate);
	return led_rate;
}

void HAL::led_timer_loop()
{
	try {
		while ( !stopped )
		{
			size_t time_to_switch = 100000;
			{
				std::unique_lock<std::mutex> lck(hal_mutex);

				if(led_rate != 0)
				{
					time_to_switch = 1000000 / (led_rate *2);
					if (led_state == LED_STATE_ON)
						led_state = LED_STATE_OFF;
					else
						led_state = LED_STATE_ON;
					printLedStatus(led_state, led_color, led_rate);
				}
			}
			usleep(time_to_switch);
		}
	}
	catch (std::exception& e)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Exception caught: \'%s\'", e.what());
		exit(1);
	}
	catch (...)
	{
		LOG_ERROR("Exception handler: file: \'%s\', line: %d, func: \'%s\'", __FILE__, __LINE__, __func__ );
		LOG_ERROR("Unknown exception!");
		exit(1);
	}
}

void HAL::run_led_timer_loop()
{
	if (!stopped) {
		LOG_INFO("LED timer loop %p already running", this);
		return;
	}
	LOG_INFO("LED timer loop %p running", this);
	stopped.exchange(false);
	inner_run();
	LOG_INFO("LED timer loop  %p running complete!", this);
}

void HAL::stop_led_timer_loop()
{
	LOG_INFO("LED timer loop %p stopping", this);
	if (!stopped) {
		stopped.exchange(true);
		inner_thread.join();
		LOG_INFO("LED timer loop %p stopping complete", this);
	}
	else
	{
		LOG_INFO("LED timer loop %p already stooped", this);
	}
}

bool HAL::is_stopped_led_timer_loop()
{
	return stopped.load();
}

}

