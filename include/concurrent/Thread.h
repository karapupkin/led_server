#ifndef INCLUDE_CONCURRENT_THREAD_H_
#define INCLUDE_CONCURRENT_THREAD_H_

#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include "logger.h"



namespace concurrent {

/// Thread base class
class Thread
{
public:
	Thread() : stopped{true}
	{
		//empty
	}

	virtual ~Thread() {}

	/// Run thread
	void run()
	{
		if (!stopped) {
			LOG_INFO("Thread %p already running", this);
			return;
		}
		LOG_INFO("Thread %p running", this);
		stopped.exchange(false);
		inner_run();
		LOG_INFO("Thread %p running complete!", this);
	}

	/// Stop thread
	void stop()
	{
		LOG_INFO("Thread %p stopping", this);
		if (!stopped) {
			stopped.exchange(true);
			inner_thread.join();
			LOG_INFO("Thread %p stopping complete", this);
		}
		else
		{
			LOG_INFO("Thread %p already stooped", this);
		}
	}

	/// Check thread is stopped
	bool is_stopped()
	{
		return stopped.load();
	}

protected:

	virtual void main_loop() {};

	void inner_run()
	{
		inner_thread = std::thread([this] {main_loop();});
		thread_id = inner_thread.get_id();
	}
	std::thread inner_thread;
	std::thread::id thread_id;
	std::atomic<bool> stopped;

};

}



#endif /* INCLUDE_CONCURRENT_THREAD_H_ */
