
#ifndef INCLUDE_CONCURRENT_QUEUE_H_
#define INCLUDE_CONCURRENT_QUEUE_H_

#include <queue>
#include <memory>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "logger.h"

namespace concurrent {

/// template queue
template <typename T>
class Queue {
	public:

		typedef std::unique_ptr<T> QueueElement;
		Queue() :
			storage_size{0}
			, stopped{true}
		{

		}

		~Queue()
		{
			if(!stopped)
				stop();
		}

		void start()
		{
			stopped.exchange(false);
			LOG_DEBUG("Queue %p start", this);
		}

		void stop()
		{
			LOG_DEBUG("Queue %p stop", this);
			stopped.exchange(true);
			std::unique_lock<std::mutex> lck(queue_mutex);
			condition.notify_all();
			LOG_DEBUG("Queue %p stopped", this);
		}

		size_t size() const
		{
			return storage_size;
		}

		///  Push new element to queue
		bool push(std::unique_ptr<T>& data)
		{
			if (!data || stopped)
				return false;

			std::unique_lock<std::mutex> lck(queue_mutex);
			storage.emplace(std::move(data));
			++storage_size;
			condition.notify_all();
			return true;
		}

		/// get element with block wait
		QueueElement get()
		{
			std::unique_lock<std::mutex> lck(queue_mutex);
			while (!stopped && storage.empty()) {
				condition.wait(lck);
			}

			if (!storage.empty())
			{
				QueueElement element(std::move(storage.front()));
				storage.pop();
				--storage_size;
				return element;
			}
			return QueueElement();
		}

		/// get element with time wait in microseconds
		std::unique_ptr<T> get_wait(int microseconds)
		{
			std::unique_lock<std::mutex> lck(queue_mutex);
			if (!stopped && storage.empty()) {
				condition.wait_for(lck,std::chrono::microseconds(microseconds));
			}

			if (!storage.empty())
			{
				QueueElement element(std::move(storage.front()));
				storage.pop();
				--storage_size;
				return element;
			}
			return QueueElement();
		}

	private:
		Queue(const Queue& queue);
		Queue& operator=(const Queue& queue);
		size_t storage_size;
		std::atomic<bool> stopped;
		std::queue<QueueElement> storage;
		std::mutex queue_mutex;
		std::condition_variable condition;
};

} //end namespace concurrent



#endif /* INCLUDE_CONCURRENT_QUEUE_H_ */
