
#ifndef INCLUDE_LOGGER_H_
#define INCLUDE_LOGGER_H_


#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>


#undef LOG_NOISE
#undef LOG_INFO
#undef LOG_DEBUG
#undef LOG_WARN
#undef LOG_ERROR
#undef LOG_FATAL


#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
	FATAL_ERROR_LEVEL,
	ERROR_LEVEL,
	WARNING_LEVEL,
	INFO_LEVEL,
	DEBUG_LEVEL,
	NOISE_LEVEL,
} logLevel_e;


void print_log(logLevel_e level, const char * format, ...) __attribute__((format(printf, 2, 3)));


#if defined(RELEASE_TARGET)
#define LOG_NOISE(fmt, ...)   ((void)0)
#define LOG_DEBUG(fmt, ...)   ((void)0)
#else
#define LOG_NOISE(fmt, ...)   print_log( NOISE_LEVEL,       "[NSE] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )
#define LOG_DEBUG(fmt, ...)   print_log( DEBUG_LEVEL,       "[DBG] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )
#endif

#define LOG_INFO(fmt, ...)    print_log( INFO_LEVEL,        "[INF] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )
#define LOG_WARN(fmt, ...)    print_log( WARNING_LEVEL,     "[WRN] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )
#define LOG_ERROR(fmt, ...)   print_log( ERROR_LEVEL,       "[ERR] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )
#define LOG_FATAL(fmt, ...)   print_log( FATAL_ERROR_LEVEL, "[FATAL] %s():"   fmt "\n", __func__, ##__VA_ARGS__ )


#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_LOGGER_H_ */
