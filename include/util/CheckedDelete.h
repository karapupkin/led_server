#ifndef INCLUDE_UTIL_CHECKEDDELETE_H_
#define INCLUDE_UTIL_CHECKEDDELETE_H_


namespace util
{

	template<class T>
	inline void type_must_be_complete()
	{
		typedef char complete_type[ sizeof(T) ? 1: -1 ];
		(void) sizeof(complete_type);
	}

	template<class T>
	inline void checked_delete(T* x)
	{
		type_must_be_complete<T>();
		delete x;
	}

	template<class T>
	inline void checked_array_delete(T* x)
	{
		type_must_be_complete<T>();
		delete [] x;
	}

}
#endif /* INCLUDE_UTIL_CHECKEDDELETE_H_ */
