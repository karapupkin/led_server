#ifndef INCLUDE_UTIL_SINGLETONBASE_H_
#define INCLUDE_UTIL_SINGLETONBASE_H_

#include <pthread.h>
#include <stdlib.h>
#include "util/CheckedDelete.h"


namespace util {

	template <typename T>
	class SingletonBase
	{
	public:
		static T& instance();
		static void prefetch();

	protected:
		SingletonBase();
		~SingletonBase();

		static void init();
		static void destroy();
	private:
		SingletonBase(const SingletonBase&);
		const SingletonBase& operator=(const SingletonBase&);

	private:
		static pthread_once_t once;
		static T *_instance;
	};

	template <typename T>
	pthread_once_t SingletonBase<T>::once = PTHREAD_ONCE_INIT;

	template <typename T>
	T* SingletonBase<T>::_instance = 0;

	template <typename T>
	SingletonBase<T>::SingletonBase()
	{
	}

	template <typename T>
	SingletonBase<T>::~SingletonBase()
	{
	}

	template <typename T>
	T& SingletonBase<T>::instance()
	{
		::pthread_once(&once, &init);

		return *_instance;
	}

	template <typename T>
	void SingletonBase<T>::prefetch()
	{
		::pthread_once(&once, &init);

	}

	template <typename T>
	void SingletonBase<T>::init()
	{
		_instance = new T();

		::atexit(&destroy);
	}

	template <typename T>
	void SingletonBase<T>::destroy()
	{
		if (_instance)
		{
			type_must_be_complete<T>();
			delete _instance;
			_instance = 0;
		}
	}
}



#endif /* INCLUDE_UTIL_SINGLETONBASE_H_ */
