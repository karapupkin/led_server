#ifndef INCLUDE_UTIL_UTIL_H_
#define INCLUDE_UTIL_UTIL_H_


#include <errno.h>
#include <string>
#include "logger.h"


namespace util {

	/// error to string conversion
	std::string str_error(int error);

	/// error to string conversion
	inline std::string str_errno() {
		return str_error(errno);
	}

	/// trim string
	std::string trim(const std::string &str);

	/// split std::string by delimeter
	template <typename ContainerType>
	inline void str_split(const std::string &str, const std::string &delim, ContainerType& result)
	{
		if (str.empty()) return;

		std::string::size_type start = 0, end;
		while ((end = str.find(delim, start)) != std::string::npos)
		{
			if (end > start)
			{
				result.push_back(std::move(std::string(str, start, end - start)));
			}
			start = end + delim.size();
		}

		if (start < str.size() - 1)
		{
			result.push_back(std::move(std::string(str, start)));
		}
	}

} // namespace util

#endif /* INCLUDE_UTIL_UTIL_H_ */
