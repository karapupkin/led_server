
#ifndef INCLUDE_NET_FIFOMESSAGE_H_
#define INCLUDE_NET_FIFOMESSAGE_H_

#include <memory>
#include "net/Message.h"

namespace command {
class Response;
}
namespace net {

/// Template for FIFO transport Message
/// template parameter can return answer to Message sender
template< class T>
class FifoMessage : public Message {
public:
	FifoMessage(std::vector<char>& command, std::unique_ptr<T> pred) :
		Message(command)
		, sender{std::move(pred)}
	{

	}

	~FifoMessage(){};

	bool answer(command::Response &response)
	{
		if (!sender)
			return false;
		return sender->sendResponse(response);
	}

private:
	std::unique_ptr<T> sender;
};

}


#endif /* INCLUDE_NET_FIFOMESSAGE_H_ */
