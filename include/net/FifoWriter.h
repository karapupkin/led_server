#ifndef INCLUDE_NET_FIFOWRITER_H_
#define INCLUDE_NET_FIFOWRITER_H_

#include <vector>
namespace command{
class Response;
}

namespace net {

/// Write answer to sender via FIFO
class FifoWriter {
public:
	FifoWriter(std::vector<char>& r_addr) : response_addr{std::move(r_addr)}
	{}

	/// Send Message response to sender
	bool sendResponse(command::Response& response);
private:
	std::vector<char> response_addr;
};
}


#endif /* INCLUDE_NET_FIFOWRITER_H_ */
