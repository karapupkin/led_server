#ifndef INCLUDE_NET_FIFOREADSERVER_H_
#define INCLUDE_NET_FIFOREADSERVER_H_

#include <thread>
#include <atomic>
#include <vector>
#include "logger.h"

namespace command {
class Response;
class CommandDispatcher;
}

namespace net {

/// FIFO reader server
class FifoReadServer
{
public:

	/// Constructor
	FifoReadServer(command::CommandDispatcher& dispatcher);
	~FifoReadServer();

	/// Run FIFO Reader thread
	void run()
	{
		if (!stopped) {
			LOG_INFO("Thread %p already running", this);
			return;
		}
		LOG_INFO("Thread %p running", this);
		stopped.exchange(false);
		inner_run();
		LOG_INFO("Thread %p running complete!", this);
	}

	/// Stop FIFO reader thread
	void stop()
	{
		LOG_INFO("Thread %p stopping", this);
		if (!stopped) {
			stopped.exchange(true);
			inner_thread.join();
			LOG_INFO("Thread %p stopping complete", this);
		}
		else
		{
			LOG_INFO("Thread %p already stooped", this);
		}
	}

	/// Check thread is stopped
	bool is_stopped()
	{
		return stopped.load();
	}

private:
	FifoReadServer();

	/// Internal function to send response to sender
	bool write_response(std::vector<char> &response_addr, command::Response& resp);

	void main_loop();

	void inner_run()
	{
		inner_thread = std::thread([this] {main_loop();});
		thread_id = inner_thread.get_id();
	}

	std::thread inner_thread;
	std::thread::id thread_id;
	std::atomic<bool> stopped;

	/// reference to Command Dispatcher
	command::CommandDispatcher& commandDispatcher;
};

}

#endif /* INCLUDE_NET_FIFOREADSERVER_H_ */
