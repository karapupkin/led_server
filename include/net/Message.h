#ifndef INCLUDE_NET_MESSAGE_H_
#define INCLUDE_NET_MESSAGE_H_
#include <vector>

namespace command {
class Response;
}
namespace net {

/// Message interface
class Message {
public:
	Message(std::vector<char>& command_) : command{std::move(command_)}
	{}
	virtual ~Message() {};

	/// Answer on message
	virtual bool answer(command::Response &response) = 0;

	/// Interface to internal data
	std::vector<char>& getData() {return command;}
private:
	Message();
	Message& operator=(const Message&);
protected:
	std::vector<char> command;
};

}

#endif /* INCLUDE_NET_MESSAGE_H_ */
