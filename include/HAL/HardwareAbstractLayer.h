#ifndef INCLUDE_HAL_HARDWAREABSTRACTLAYER_H_
#define INCLUDE_HAL_HARDWAREABSTRACTLAYER_H_

#include <mutex>
#include <thread>
#include <atomic>
#include <functional>

#include "util/SingletonBase.h"

namespace hardware {

/// Support LED Colors
enum LedColor {
	LED_COLOR_RED = 1,
	LED_COLOR_GREEN,
	LED_COLOR_BLUE
};

/// Support LED state
enum LedState {
	LED_STATE_ON = 1,
	LED_STATE_OFF,
};

/// Singletone Hardware Abstract layer
class HAL : public util::SingletonBase<HAL>
{
public:
	/// Set LED state on/off
	bool setLedState(enum LedState state);

	/// Get LED state on/off
	LedState getLedState();

	/// Set LED color
	bool setLedColor(enum LedColor color);

	/// Get LED color
	LedColor getLedColor();

	/// Set LED rate
	bool setLedRate(size_t rate);

	/// Get LED rate
	size_t getLedRate();

private:
	friend class util::SingletonBase<HAL>;
	HAL();
	~HAL();

	/// HAL soft timer loop for LED blink
	void led_timer_loop();

	void inner_run()
	{
		inner_thread = std::thread([this]{led_timer_loop(); });
		thread_id = inner_thread.get_id();
	}

	void run_led_timer_loop();

	void stop_led_timer_loop();
	bool is_stopped_led_timer_loop();

	std::thread inner_thread;
	std::thread::id thread_id;
	std::atomic<bool> stopped;

	/// mutex access guard for HAL elements
	std::mutex hal_mutex;

	/// LED state storage
	LedState led_state;
	/// LED color storage
	LedColor led_color;
	/// LED rate storage
	size_t led_rate;

};

} // end namespace hardware

#endif /* INCLUDE_HAL_HARDWAREABSTRACTLAYER_H_ */
