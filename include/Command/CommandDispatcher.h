#ifndef INCLUDE_COMMAND_COMMANDDISPATCHER_H_
#define INCLUDE_COMMAND_COMMANDDISPATCHER_H_
#include <memory>
#include "concurrent/Queue.h"
#include "concurrent/Thread.h"
#include "net/Message.h"

namespace command
{
class CommandManager;

/// Command Dispatcher with own thread
class CommandDispatcher : public concurrent::Thread
{
public:

	CommandDispatcher();
	~CommandDispatcher();

	/// interface to add new Message for dispatch
	bool addMessage(std::unique_ptr<net::Message>& mes);

protected:

	/// dispatcher thread function
	void main_loop();

private:
	/// Message queue typedef
	typedef concurrent::Queue<net::Message> MessageQueue;
	/// Message queue
	MessageQueue messageQueue;
	/// Command manager with all known commands
	std::unique_ptr<CommandManager> commandManager;
};

} // end namespace command

#endif /* INCLUDE_COMMAND_COMMANDDISPATCHER_H_ */

