#ifndef INCLUDE_COMMAND_H_
#define INCLUDE_COMMAND_H_

#include <vector>
#include "CommandResponse.h"


namespace command {

/// Abstract Command class
class Command
{
public:
	virtual ~Command() {}

	/// Command name
	virtual const char* name() = 0;

	/// Execute command with args
	virtual void execute(std::vector<const char*>& args, Response& resp) = 0;
};

} // end namespace command

#endif /* INCLUDE_COMMAND_H_ */
