#ifndef INCLUDE_GETLEDCOLORCOMMAND_H_
#define INCLUDE_GETLEDCOLORCOMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for Get LED color command
class GetLedColorCommand : public Command
{
protected:

public:
	GetLedColorCommand();
	virtual ~GetLedColorCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command


#endif /* INCLUDE_GETLEDCOLORCOMMAND_H_ */
