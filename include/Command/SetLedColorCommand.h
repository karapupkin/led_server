#ifndef INCLUDE_SETLEDCOLORCOMMAND_H_
#define INCLUDE_SETLEDCOLORCOMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for set LED color command
class SetLedColorCommand : public Command
{
protected:

public:
	SetLedColorCommand();
	virtual ~SetLedColorCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command

#endif /* INCLUDE_SETLEDCOLORCOMMAND_H_ */
