#ifndef INCLUDE_COMMAND_COMMANDPROTOCOL_H_
#define INCLUDE_COMMAND_COMMANDPROTOCOL_H_

#include <vector>
#include <map>
#include <string>
#include <memory>


namespace command
{

/// Command protocol parser
class CommandProtocol
{
public:
	CommandProtocol();
	~CommandProtocol();

	/// parse incoming buffer to protocol elements
	bool getCommand(std::vector<char> & buffer, size_t& offset, std::vector<char> & response_addr, std::vector<char> &command);
private:

};

} // end namespace command

#endif /* INCLUDE_COMMAND_COMMANDPROTOCOL_H_ */

