#ifndef INCLUDE_GETLEDSTATECOMMAND_H_
#define INCLUDE_GETLEDSTATECOMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for Get LED state command
class GetLedStateCommand : public Command
{
protected:

public:
	GetLedStateCommand();
	virtual ~GetLedStateCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command

#endif /* INCLUDE_GETLEDSTATECOMMAND_H_ */
