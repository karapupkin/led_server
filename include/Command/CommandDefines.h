#ifndef INCLUDE_COMMAND_COMMANDDEFINES_H_
#define INCLUDE_COMMAND_COMMANDDEFINES_H_

/// Protocol field defines

#define COMMAND_SET_LED_STATE "set-led-state"
#define COMMAND_GET_LED_STATE "get-led-state"
#define COMMAND_SET_LED_COLOR "set-led-color"
#define COMMAND_GET_LED_COLOR "get-led-color"
#define COMMAND_SET_LED_RATE "set-led-rate"
#define COMMAND_GET_LED_RATE "get-led-rate"

#define COMMAND_STATUS_OK "OK"
#define COMMAND_STATUS_FAILED "FAILED"

#define COMMAND_SET_LED_COLOR_RED "red"
#define COMMAND_SET_LED_COLOR_GREEN "green"
#define COMMAND_SET_LED_COLOR_BLUE "blue"

#define COMMAND_SET_LED_STATE_ON "on"
#define COMMAND_SET_LED_STATE_OFF "off"




#endif /* INCLUDE_COMMAND_COMMANDDEFINES_H_ */
