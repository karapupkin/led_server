#ifndef INCLUDE_SETLEDSTATECOMMAND_H_
#define INCLUDE_SETLEDSTATECOMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for set LED state command
class SetLedStateCommand : public Command
{
protected:

public:
	SetLedStateCommand();
	virtual ~SetLedStateCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command

#endif /* INCLUDE_SETLEDSTATECOMMAND_H_ */
