#ifndef INCLUDE_COMMAND_COMMANDMANAGER_H_
#define INCLUDE_COMMAND_COMMANDMANAGER_H_

#include <vector>
#include <map>
#include <string>
#include <memory>


namespace command
{

class Command;
class Response;

/// Command Storage
class CommandManager
{
public:
	CommandManager();
	~CommandManager();

	/// Init protocol commands
	void initProtocol();

	/// Execute command and generate response
	bool executeCommand(std::vector<char> & command, Response& resp);
private:
	/// command storage
	std::map<std::string, std::unique_ptr<Command>> commands;
};


} // end namespace command

#endif /* INCLUDE_COMMAND_COMMANDMANAGER_H_ */
