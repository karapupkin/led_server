#ifndef INCLUDE_SETLEDRATECAMMAND_H_
#define INCLUDE_SETLEDRATECAMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for set LED rate command
class SetLedRateCommand : public Command
{
protected:

public:
	SetLedRateCommand();
	virtual ~SetLedRateCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command

#endif /* INCLUDE_SETLEDRATECAMMAND_H_ */
