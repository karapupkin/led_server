#ifndef INCLUDE_COMMAND_COMMANDSTRINGPARSER_H_
#define INCLUDE_COMMAND_COMMANDSTRINGPARSER_H_
#pragma once

#include <vector>
#include <algorithm>
#include <string.h>
#include "logger.h"


namespace command {

	inline bool is_delim(const char* delim, char ch)
	{
		return (strchr(delim, ch) != 0);
	}

	inline bool parse_command_string(std::vector<char>& command, std::vector<const char*>& args)
	{
		args.clear();

		const char delim[] = " \t";

		for (char* str = command.data(); *str; )
		{
			str += strspn(str, delim);
			if (*str == '\0')
			{
				break;
			}

			char* saveptr;
			if (*str == '\"')
			{
				saveptr = ++str;
				str = strchr(str, '\"');
				if (!str)
					return false;

				if (str[1] != '\0' && !is_delim(delim, str[1]))
					return false;
			}
			else
			{
				saveptr = str;
				str += strcspn(str, delim);
			}

			if (*str)
				*str++ = '\0';

			args.push_back(saveptr);
		}

		return !args.empty();
	}

	inline bool parse_protocol_command(std::vector<char>& buffer, size_t& offset, std::vector<char>& command, std::vector<char>& response_addr )
	{
		auto it_command_end = std::find(buffer.begin() + offset,buffer.end(), '\n');
		if (it_command_end == buffer.end())
		{
			return false;
		}

		auto it_response_end = std::find(buffer.begin() + offset, it_command_end, ' ');
		if (it_response_end == it_command_end)
		{
			return false;
		}

		response_addr.clear();
		response_addr.assign(buffer.begin() + offset, it_response_end+1);
		response_addr[response_addr.size() - 1] = '\0';

		command.clear();
		command.assign(it_response_end + 1, it_command_end+1);
		command[command.size() - 1] = '\0';
		offset = it_command_end + 1 - buffer.begin();

		return !command.empty() && !response_addr.empty();
	}

} // namespace command
#endif /* INCLUDE_COMMAND_COMMANDSTRINGPARSER_H_ */
