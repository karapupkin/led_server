#ifndef INCLUDE_GETLEDRATECOMMAND_H_
#define INCLUDE_GETLEDRATECOMMAND_H_

#include "Command/Command.h"

namespace command {

/// handler for Get LED rate command
class GetLedRateCommand : public Command
{
protected:

public:
	GetLedRateCommand();
	virtual ~GetLedRateCommand();
	virtual const char* name();
	virtual void execute(std::vector<const char*>& args, Response& resp);
private:
	const char* cmd_name;
};

} // end namespace command

#endif /* INCLUDE_GETLEDRATECOMMAND_H_ */
