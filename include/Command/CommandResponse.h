#ifndef INCLUDE_COMMAND_COMMANDRESPONSE_H_
#define INCLUDE_COMMAND_COMMANDRESPONSE_H_

#include <string>

namespace command {

/// Response on command
class Response
{
public:
	std::string commandResponse;
};

} // end namespace command

#endif /* INCLUDE_COMMAND_COMMANDRESPONSE_H_ */
