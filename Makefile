CC=g++
CFLAGS=-std=c++11 -c -Wall
CXXFLAGS = -pthread -Wall
LDFLAGS=-pthread

# Project name
PROJECT = led_server

# Directories
OBJDIR = obj
SRCDIR = src
BINDIR = bin
SCRIPTDIR = script

EXECUTABLE=$(BINDIR)/$(PROJECT)

$(if $(BINDIR),$(shell mkdir -p $(BINDIR)),)
$(if $(OBJDIR),$(shell mkdir -p $(OBJDIR)),)

LIBS =-lpthread

ifeq ($(BUILD),debug)   
# "Debug" build - no optimization, and debugging symbols
CFLAGS += -O0 -g
CXXFLAGS += -O0 -g
PROJECT_DEFINES += DEBUG_TARGET

else
# "Release" build - optimization, and no debug symbols
CFLAGS += -O3 -s -DNDEBUG
CXXFLAGS += -O3 -s -DNDEBUG
PROJECT_DEFINES += RELEASE_TARGET

endif

PROJECT_DEFINES += 

PROJECT_INCLUDES += \
    ./include


# Files and folders
SRCS    = $(shell find $(SRCDIR) -name '*.cpp')
#SRCDIRS = $(shell find . -name '*.cpp' | dirname {} | sort | uniq | sed 's/\/$(SRCDIR)//g' )
SRCDIRS = $(shell find $(SRCDIR) -type d | sed 's/$(SRCDIR)/./g' )
OBJS    = $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SRCS))


# Targets
all:$(EXECUTABLE)

$(EXECUTABLE): buildrepo $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o $@
	cp -r $(SCRIPTDIR)/* $(BINDIR)


$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) $(CXXFLAGS) $< -o $@ $(addprefix -D,$(PROJECT_DEFINES))  $(addprefix -I,$(abspath $(PROJECT_INCLUDES))) $(LDFLAGS)

    
clean:
#    rm $(PROJECT) $(OBJDIR) -Rf
	$(RM) $(OBJS) $(deps) $(EXECUTABLE)

    
buildrepo:
	@$(call make-repo)

# Create obj directory structure
define make-repo
	mkdir -p $(OBJDIR)
	for dir in $(SRCDIRS); \
	do \
	    mkdir -p $(OBJDIR)/$$dir; \
	done
endef



